
export const colors = {
    text: {
      cardText: "#828282",
      lightText: "#BDBDBD",
    },
   
    brand: {
      primary: "#FF63DE",
      secondary: "#333333",
      muted: "#C6DAF7",
    },
   
    bg: {
      primary: "#FFFFFF",
      secondary: "#F1F1F1",
    },

    textColor: {
      strong: "#A2A2A2",
      regular: "#b5b5b5",
      soft: "#c7c7c7",
      light: "#dadada",
    },
    red: {
      strong: "#FA1E0E",
      regular: "#fb4b3e",
      soft: "#fd8f87",
      light: "#fd8f87",
    },
    black: {
      strong: "#000",
      regular: "#4d4d4d",
      soft: "#808080",
      light: "#b3b3b3",
    },
 
 
  };