import React from "react"
import { CustomButton } from "../../../components/Button"
import {Text,H1} from "../../../components/Text";
import {AiOutlineDownload} from "react-icons/ai"

import {TiArrowForward} from "react-icons/ti"

import Marv from "../../../assets/marve.png"
import {
Container,
// MarvellousMajorText
}
from "./styles"

export const Intro =()=>{
    return(
        <Container>
            <div className="section1">
                <Text size="16px" color="red">Marvellous Major </Text>
                <div className="userExperienceDesignAndResearchContainer">
                    <H1>User Experience Designer And Researcher</H1>
                    <Text 
                    size="16px"
                    weight="100"
                    >

                    Lorem ipsum dolor sit 
                    amet, consetetur sadipscing elitr,
                     sed diam nonumy eirmod tempor invidunt 
                     ut labore et dolore magna aliquyam erat,
                      sed diam voluptua. At vero eos et
                    accusam et justo duo dolores et ea rebum. Stet clita kasd"
                    
                    </Text>
                </div>
                <CustomButton>
                    <div>

                    <AiOutlineDownload size={18}/>
                    </div>
               <p>Download CV</p>
                </CustomButton>
            </div>
            <div className="section2">
                <img src={Marv} height="550px"  alt="Marv"/>
            </div>
                <div className="projectIHaveWorkedOnContainer">
            <div className="projectIHaveWorkedOn">
                <p>Projects I've Worked On </p>
                <TiArrowForward size={24}/>


                </div>
                
            </div>

        </Container>

    )
}