import styled from "styled-components"


export const Container = styled.div`
position: relative;
width:90%;


display: flex;
flex-direction: row;
justify-content: center;
flex-wrap:wrap;

/* padding-left: 40px;
padding-right: 40px; */


.section1{
    width:35%;
    
    .userExperienceDesignAndResearchContainer{
    width: 475px;

    .HeaderText{
letter-spacing:10px;
color:#000000;
/* font-size:60px; */
font-weight:bold;
font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;


    }

    

}
};
.section2{
    justify-content: center;
    align-items: center;
    display: flex;
    height: 630px;
    width:65%;
};
.projectIHaveWorkedOnContainer{
    position: absolute;
    top:450px;
    right:-70px;



        .projectIHaveWorkedOn{
        background: red;
        width:240px;
        display: flex;
        justify-content: center;
    align-items: center;
        height: 100px;
    }

    }


@media screen and (max-width:768px) {
    padding-right: 0;
    padding-left: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;





.section1{
    width:100%;
    
    .userExperienceDesignAndResearchContainer{
    width: 100%;
    display: flex;
    flex-direction: column;
    /* text-align:center; */
   
  
    

    .HeaderText{
letter-spacing:10px;
    }

    

}
};
.section2{
    justify-content: center;
    align-items: center;
    display: flex;
    /* height: 630px; */
    width:100%;

    img{
        height: 450px;
    }
};

.projectIHaveWorkedOnContainer{
    position: absolute;
    top:760px;
    right:40px;
    display:none;
    background-color: orange;



        .projectIHaveWorkedOn{
        /* background: red; */
        width:240px;
        display: flex;
        justify-content: center;
    align-items: center;
        height: 100px;
    }
}


}

   
 
    
`

