import React from 'react'
import { Container } from './styles'

import { NavBar } from '../../components/NavBar/index'
import { Intro } from './Intro'
import Research from './Research'
import {Work} from './Works'
import  {WhatIDo}  from './What-I-Do'
// import { DesignApproach } from './DesignApproach'
import { ContactMe } from './ContactMe'
import { Footer } from '../../components/Footer'

const HomePage = () => {
  return (
    <Container>
      <NavBar />
      <Intro />
       <Research />
      <Work />
        <WhatIDo />
        <ContactMe/>
        <Footer/> 
    </Container>
  )
}
export default HomePage
