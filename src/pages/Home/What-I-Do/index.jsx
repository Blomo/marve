import React from 'react'
import { Container,  } from './styles'
import Image from '../../../assets/web2.png'

import Image1 from '../../../assets/mobile1.png'

export const WhatIDo = () => {
  return (
    <Container>
      <div className="title">
        
        <div className="whatIDo">
          <p className="orangeLine"></p>
          <p>What I Do</p>
        </div>
        <div className="myApproach">
          <h1>My Approach</h1>
        </div>
      </div>

      <div className="cardContainer">
        <img src={Image} alt="sdd" className="image"/>
        <img src={Image1} alt="sdd" className="image1"/>
        
      </div>
    </Container>
  )
}
