import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;

  .title{
    display:flex;
    flex-direction:column;


    .whatIDo{
      display:flex;
    align-items:center;
    justify-content:flex-end;
    color:#F24A32;


    .orangeLine{
      width:200px;
      height:5px;
      align-content:center;
      text-align:center;
      margin-top:25px;
      border-radius:10px;
      background-color:#F24A32;
    }

  }
   .myApproach{
      display:flex;
    align-content:flex-end;
    align-items:flex-start;
    height:max-content;
    justify-content:flex-end;

  }

  }
  .cardContainer{
    display:flex;
    align-content:center;
    justify-content:center;

    .image1{
      display:none
    }
  }


  @media screen and (max-width:768px) {
      width: 100%;
.cardContainer{
    display:flex;
    align-content:center;
    justify-content:center;

       .image{
      display:none;
    }

    .image1{
      display:flex;
    }
  }
}

  
`

