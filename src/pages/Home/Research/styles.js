
import styled from 'styled-components'

export const Container = styled.div`
justify-content:center;
width: 80%;
display:flex;



@media screen and (max-width:768px) {
      width: 90%;
}

.picturesContainer{
  width:100%;
  display:flex;
  justify-content:space-around;


  @media screen and (max-width:768px) {
      width: 100%;

  }
  
}
`