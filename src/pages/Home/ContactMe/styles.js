import styled from "styled-components";
export const Container = styled.div`
  width: 100%;

  .section1 {
    width: 100%;

    display: flex;
    .contact-me {
      color: #f24a32;
    }
    .orangeLine {
      width: 800px;
      height: 5px;
      align-content: center;
      text-align: center;
      margin-top: 25px;
      margin-left:70px;
      border-radius: 10px;
      background-color: #f24a32;
    }
  }

  .section2 {
    display: flex;
    justify-content: center;
    padding-left:70px;
    flex-direction: column;
    .myApproach {
      display: flex;
      width: 80%;
      height: max-content;
      justify-content: flex-end;
    }
  }

  .label {
    width: 25%;
    align-items: flex-end;
    justify-content: flex-end;
    display: flex;

    @media screen and (max-width: 768px) {

width: 30%;
justify-content: center;
font-size:12px;
display: flex;
align-items: center;


}
  }

  input {
    background-color: transparent;
    border: none;
    width: 40%;
    border-bottom: 1px solid;

    @media screen and (max-width: 768px) {

width: 60%;
justify-content: center;
display: flex;
align-items: center;


}
  }

  .button-container {
    width: 70%;
    height: 100px;
    justify-content: flex-end;
    display: flex;
    align-items: center;




  }

  @media screen and (max-width: 768px) {
.button-container{
width: 100%;
justify-content: center;
display: flex;
flex-direction: column;
align-items: center;

}
}
`;
export const Form = styled.div`
  width: 100%;
  align-items: center;
  display: flex;
  justify-content: center;


  @media screen and (max-width: 768px) {

width: 100%;
justify-content: center;
display: flex;
align-items: center;


}
`;
