import React from "react";
import { Container,Form } from "./styles";
import { H1 } from "../../../components/Text/index";
import { CustomButton } from "../../../components/Button";

export const ContactMe = () => {
  return (
    <Container>
      <div className="section1">
        <div className="orangeLine"></div>
        <p className="contact-me">contact me</p>
      </div>
      <div className="section2">
        <div className="myApproach">
          <H1>For Any Inquiry Or Just To Say Hello Reach Out Here</H1>
        </div>
        <div>
          <Form>
            <p className="label">Full Name:</p>
            <input className="input" />
          </Form>
          <Form>
            <p className="label">Email</p>
            <input className="input" />
          </Form>
          <Form>
            <p className="label">Company</p>
            <input className="input" />
          </Form>
          <Form>
            <p className="label">How did you get to me</p>
            <input className="input" />
          </Form>
          <Form>
            <p className="label">Cool! Let me know your message:</p>
            <input className="input" />
          </Form>


          <div className="button-container">
              <CustomButton>Send Message</CustomButton>
          </div>
        </div>
      </div>
    </Container>
  );
};
