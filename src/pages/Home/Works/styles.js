import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction:column;
  align-items: center;


  .cardContainer {
    width: 90%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`
export const CardWrapper = styled.div`
  background: url(${({ Image }) => (Image ? Image : Image)});
  width: 48%;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  height: 420px;
  justify-content: center;
  display: flex;
  border-radius: 10px;
  /* background-color:yellow; */
  margin-bottom:10px;
  align-items: flex-end;
  overflow:hidden;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  /* text-align: center; */


  @media screen and (max-width:768px) {
      width: 100%;
        height: 220px;

}
`

export const Name = styled.div`
  width: 100%;
 
  height: 70px;
  background-color: white;
  justify-content: space-around;





@media screen and (max-width:768px) {

}
`
