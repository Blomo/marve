import React from 'react'
import { Container, CardWrapper, Name } from './styles'
import Cover from '../../../assets/number2.png'
import Cover2 from '../../../assets/2.jpeg'

import Cover3 from '../../../assets/3.jpeg'

export const Work = () => {
  const Cards = [
    {
      Image: Cover,
      name: '',
    },
    {
      Image: Cover2,
      name: 'dd',
    },
    {
      Image: Cover3,
      name: 'ddd',
    },
    {
      Image: Cover,
      name: '',
    },
  ]
  return (
    <Container>
      <p>Loved Project</p>
      <h1>Works</h1>
      <div className="cardContainer">
        {Cards.map(({ Image, name }) => (
          <CardWrapper className="Card" Image={Image}>
            <Name>
              <p>{name}</p>
            </Name>
          </CardWrapper>
        ))}
      </div>
    </Container>
  )
}
