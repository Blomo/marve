import React from 'react'
import { Container,  } from './styles'

import Image1 from '../../../assets/web1.png'

export const DesignApproach = () => {
  return (
    <Container>
      <div className="title">
        
        <div className="whatIDo">
          <p className="orangeLine"></p>
          <p>When Idea is New</p>
        </div>
        <div className="myApproach">
          <h1>Design Approach</h1>
        </div>
      </div>

      <div className="cardContainer">
        <img src={Image1} alt="sdd" className="image1"/>
        
      </div>
    </Container>
  )
}
