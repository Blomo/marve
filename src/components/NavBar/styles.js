import styled from  "styled-components"
import {NavLink as Link } from "react-router-dom"

export const Container =styled.div`
height: 60px;
display: flex;
width: 100%;
display: flex;
justify-content: center;
align-items: center;


.InnerContainer{
height: 60px;
justify-content: space-between;
display: flex;
padding-right: 40px;
width: 100%;
align-items:center;
z-index:10;
padding-left: 70px;
padding-right: 40px; 



.select{
justify-content:space-around;
display: flex;
width:30%;
align-items:center;

};
}
.bar{
    display: none;
    color:#000000
};
@media screen and (max-width:768px) {
    padding-left:0;
    padding-right:-110;
    padding-top:10px;
    justify-content: flex-end;
    padding-bottom:10px;
    height: auto;
    .InnerContainer{
    display: none;
}
.bar{ 
 display: block;
 /* position: absolute;
 top:0;
 right: 0;
 padding-left:0;
 padding-right:0;
 transform: translate(-100%, 75%);
 font-size:1.8rem;
 cursor:pointer */
}
}


`

export const  NavLink =styled(Link)`
color:black;
display: flex;
align-items: center;
text-decoration:none;
padding: 0 1rem;
height:100%auto;
cursor: pointer;

&.active{
    color:#15cdfc 
}





`