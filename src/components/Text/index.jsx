import React from "react";
import styled from "styled-components";
// import { colors } from "../../infrastructure/theme/color";

export const Text = (props) => {
  return (
    <PText color={props.color} weight={props.weight} size={props.size}>
      {props.children}
    </PText>
  );
};

export const H1 = (props) => {
  return (
    <H1Text color={props.color} weight={props.weight} size={props.size}>
      {props.children}
    </H1Text>
  );
};

const PText = styled.p`
  color: ${({ color }) => (color ? color : "#000000")};
  font-size: ${({ size }) => (size ? size : "20px")};
  margin: 0px;
  margin-bottom: 10px;
  font-weight: ${({ weight }) => (weight ? weight : "bold")};
  font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;
`;

const H1Text = styled.p`
  height: auto;
  color: ${({ color }) => (color ? color : "#000000")};
  font-size: ${({ size }) => (size ? size : "60px")};
  margin: 0px;
  margin-bottom: 10px;
  font-weight: ${({ weight }) => (weight ? weight : "bold")};

  letter-spacing: -2px;
  font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;

  @media screen and (max-width: 768px) {
    /* text-align:center; */
    /* background-color: aliceblue; */
    font-size: 40px;
  }
`;
