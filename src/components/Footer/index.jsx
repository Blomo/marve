import React from "react";
import {FooterContainer} from "./styles"
import  Dribble from "../../assets/dribbble.png"
import  Benance from "../../assets/benance.png"


export const Footer = () =>{
    return(
        <FooterContainer>
            <div className="section1">
                <div className="my-work">my Works Are Featured in</div>
                <br/>
                <div className="">
                    <img src={Dribble } alt="dddd"/>
                    <img src={ Benance} alt="dddd"/>
                    
                </div>
            </div>
            <div className="section2">
                <p>Marvellous Major. All rights reserved 2021</p>
            </div>

        </FooterContainer>
    )
}