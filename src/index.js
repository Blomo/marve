import React from 'react';
import ReactDOM from 'react-dom';
import "./index.css"
// import {NavBar} from "../src/components/NavBar/index.jsx"
import HomePage from './pages/Home';


ReactDOM.render(
  <React.StrictMode>
    <HomePage/>
  </React.StrictMode>,
  document.getElementById('root')
);

